#lang racket

; Exercise 1.4
  (define (a-plus-abs-b a b)
    ((if (> b 0) + -) a b))
; The procedure a-plus-abs-b sums a and the absolute value of b
