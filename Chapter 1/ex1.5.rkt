#lang racket

; Exercise 1.5
(define (p) (p))

(define (test x y)
  (if (= x 0)
      0
      y))
#| If the interpreter uses applicative order evaluation the interpreter would get stuck in an infinite loop and not return anything
If the interpreter uses normal order evaluation the interpreter would return 0.  The reason for this is because in
Applicative Order Evaluation the opperands are evalutated first and thus they would go in to an infinite loop.
 In Normal Order Evaluation, the expression is expanded first and the operands are not evaluated until their values are needed  |#
